﻿// Напишите программу, которая будет преобразовывать сумму, записанную в старом формате, в новый формат(1 фунт = 20 шил.; 1 шил. = 12 пенс.)

#include "pch.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(0, "");
	int f, s, p;
	cout << "Введите кол-во фунтов: ";
	cin >> f;
	cout << "Введите кол-во шилленгов: ";
	cin >> s;
	cout << "Введите кол-во пенсов: ";
	cin >> p;
	cout << "Десятичных фунтов: " << f + ((static_cast <float>(s) * 12 + p) / 240) << endl;
	system("pause");
	return 0;
}

